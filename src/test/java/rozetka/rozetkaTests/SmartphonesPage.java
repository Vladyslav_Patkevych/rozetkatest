package rozetka.rozetkaTests;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.LinkedList;

class SmartphonesPage {

    private ChromeDriver driver;
    private WebDriverWait wait;

    SmartphonesPage(ChromeDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    private By titleOfElementInList = By.className("g-i-list-title");
    private By priceOfElementInList = By.className("g-price-uah");
    private By imageOfElementInList = By.className("g-i-list-img");
    private By popularityIcon = By.className("g-tag-icon-small-popularity");

    void topSales() {
        int listSizeByTitles = driver.findElements(titleOfElementInList).size();
        int listSizeByPrices = driver.findElements(priceOfElementInList).size();
        int amountOfPopularGoodsOnCurrentPage;

        Assert.assertEquals(listSizeByPrices, listSizeByTitles);

        LinkedList<String> titles = new LinkedList<String>();
        LinkedList<String> prices = new LinkedList<String>();
        LinkedList<String> popularity = new LinkedList<String>();
        boolean checkPopularityLabel;

        for (int page = 1; page <= 3; page++) {

            amountOfPopularGoodsOnCurrentPage =
                    driver.findElements(popularityIcon).size();

            for (int i = 0; i < listSizeByTitles; i++) {

                try {
                    driver.findElements(imageOfElementInList).get(i)
                            .findElement(popularityIcon);
                    checkPopularityLabel = true;
                } catch (Exception e) {
                    checkPopularityLabel = false;
                }

                titles.add(driver.findElements(titleOfElementInList).get(i).getText());
                prices.add(driver.findElements(priceOfElementInList).get(i).getText());
                popularity.add(Boolean.toString(checkPopularityLabel));

                if (popularity.get(i).equals("true")) {
                    System.out.println(
                            "Title: " + titles.get(i) +
                                    "\nPrice: " + prices.get(i) + "\n"
                    );
                }
            }

            titles.clear();
            prices.clear();
            popularity.clear();

            System.out.println(
                    "Amount of popular goods on page number " + page + ": " + amountOfPopularGoodsOnCurrentPage + "\n"
            );

            driver.findElement(By.id("page"+(page+1))).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"page" + (page+1) + "\"]/span")));

        }
    }
}

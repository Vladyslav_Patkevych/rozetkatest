package rozetka.rozetkaTests;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

class MainPage {

    private ChromeDriver driver;
    private WebDriverWait wait;

    MainPage(ChromeDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    private By smartphonesTvs = By.id("3361");
    private By phones = By.cssSelector("li:nth-child(1) > p > a");
    private By smartphones = By.cssSelector("li:nth-child(1) > div > a.m-cat-l-i-title-link");
    private By viewList = By.id("filter_viewlist");
    private By titleOfFirstElementInList = By.className("g-i-list-title");

    private String mainPageUrl = "https://rozetka.com.ua/";
    private String expectedTitle = "Интернет-магазин ROZETKA™: фототехника, " +
            "видеотехника, аудиотехника, компьютеры и компьютерные комплектующие";

    void openMainPage() {
        driver.get(mainPageUrl);
    }

    void startCheck() {

        String actualTitle = driver.getTitle();

        Assert.assertEquals(expectedTitle, actualTitle);
    }

    void selectCategory() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(smartphonesTvs));
        driver.findElement(smartphonesTvs).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(phones));
        driver.findElement(phones).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(smartphones));
        driver.findElement(smartphones).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(viewList));
        driver.findElement(viewList).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(titleOfFirstElementInList));
    }

}

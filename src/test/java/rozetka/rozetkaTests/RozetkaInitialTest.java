package rozetka.rozetkaTests;

import org.junit.Test;
import rozetka.ChromeDriverSettings;

public class RozetkaInitialTest extends ChromeDriverSettings {

    @Test
    public void rozetkaInitialTest() {

        MainPage mainPage = new MainPage(driver);
        SmartphonesPage smartphonesPage = new SmartphonesPage(driver);

        mainPage.openMainPage();

        mainPage.startCheck();

        mainPage.selectCategory();

        smartphonesPage.topSales();

    }
}